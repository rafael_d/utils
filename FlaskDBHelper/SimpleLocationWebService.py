from flask import Flask, request, g
from flask_restful import Resource, Api, reqparse
from DBHelper import DBHelper

DATABASE = 'experimental.db'
app = Flask(__name__)
db = DBHelper(DATABASE, g, True)
api = Api(app)


class GetBSSIDs(Resource):
    def get(self):
        rows = db.query('BSSID_LOCATION')

        return {'bssids': rows}


class ManipulateBSSID(Resource):
    def get(self, id):
        row = db.query('BSSID_LOCATION', None, 'ID =?', (str(id),))

        return {'bssid':row}

    def delete(self, id):
        result = db.delete('BSSID_LOCATION', 'ID =?', (str(id),))

        return {'result': result}, 200


class InsertBSSID(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('bssid', type=str, help='BSSID identifier')
        parser.add_argument('latitude', type=str, help='Latitude information')
        parser.add_argument('longitude', type=str, help='Longitude information')
        args = parser.parse_args()

        result = db.insert('BSSID_LOCATION', args.keys(), args.values())

        return {'result': result}, 200


if __name__ == '__main__':
    api.add_resource(GetBSSIDs, '/locweb/api/ids')
    api.add_resource(ManipulateBSSID, '/locweb/api/id/<int:id>')
    api.add_resource(InsertBSSID, '/locweb/api/bssid')

    app.run(debug=True)
