import cwiid
import time


def main():
    button_delay = 0.5
    rumble_delay = 1
    acc_delay = 0.5

    print 'Press 1 + 2 on your Wii Remote now ...'
    time.sleep(1)

    # Connect to the Wii Remote. If it times out
    # then quit.
    try:
        wii = cwiid.Wiimote()
    except RuntimeError:
        print "Error opening wiimote connection"
        quit()

    print 'Wii Remote connected...\n'
    wii.rumble = False
    wii.rumble = True
    time.sleep(0.1)
    wii.rumble = False
    time.sleep(2)
    wii.led = cwiid.LED1_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON + cwiid.LED2_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON + cwiid.LED2_ON + cwiid.LED3_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON + cwiid.LED2_ON + cwiid.LED3_ON + cwiid.LED4_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON + cwiid.LED2_ON + cwiid.LED3_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON + cwiid.LED2_ON
    time.sleep(0.1)
    wii.led = cwiid.LED1_ON
    time.sleep(0.1)
    wii.led = 0
    print 'Press some buttons!\n'
    print 'Press A and B together to acelerometer.'
    print 'Press HOME to rumble.'
    print 'Press PLUS and MINUS together to disconnect and quit.\n'

    wii.rpt_mode = cwiid.RPT_BTN
    acc_enabled = False
    rumble = False

    while True:
        buttons = wii.state['buttons']

        if buttons & cwiid.BTN_HOME:
            print 'HOME'
            rumble = not rumble
            wii.rumble = rumble
            time.sleep(rumble_delay)

        if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS) == 0:
            print 'Closing connection'
            wii.rumble = False
            wii.rumble = True
            time.sleep(0.1)
            wii.rumble = False
            time.sleep(0.1)
            wii.rumble = True
            time.sleep(0.1)
            wii.rumble = False

            exit(wii)

        if (buttons - cwiid.BTN_A - cwiid.BTN_B) == 0:
            acc_enabled = not acc_enabled
            wii.rpt_mode = cwiid.RPT_ACC | cwiid.RPT_BTN if acc_enabled else cwiid.RPT_BTN

        if buttons & cwiid.BTN_LEFT:
            wii.led = wii.state['led'] ^ cwiid.LED1_ON
            print 'LED'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_UP:
            wii.led = wii.state['led'] ^ cwiid.LED2_ON
            print 'UP'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_DOWN:
            wii.led = wii.state['led'] ^ cwiid.LED3_ON
            print 'DOWN'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_RIGHT:
            wii.led = wii.state['led'] ^ cwiid.LED4_ON
            print 'RIGHT'
            time.sleep(button_delay)

        if buttons & cwiid.BTN_1:
            print 'BTN_1'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_2:
            print 'BTN_2'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_A:
            print 'BTN_A'
            time.sleep(button_delay)
        if buttons & cwiid.BTN_B:
            print 'BTN_B'
            time.sleep(button_delay)

        if acc_enabled:
            acc = wii.state['acc']
            print 'x=%s, y=%s, z=%s' % acc
            time.sleep(acc_delay)

if __name__ == '__main__':
    main()